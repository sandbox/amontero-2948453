<?php

/**
 * @file
 * Implements the HookedQueue class.
 */

class HookedQueue implements DrupalQueueInterface {

  const VAR_PREFIX = 'hooked_queue_';

  /**
   * The name of the queue this instance is working with.
   *
   * @var string
   */
  protected $name;

  /**
   * The wrapped queue object.
   *
   * @var DrupalQueue
   */
  protected $queue;

  public function __construct($name) {
    $this->name = $name;
    $this->queue = $this->getWrappedQueue($name);
  }

  /**
   * Returns the wrapped queue object.
   *
   * @see DrupalQueue::get()
   */
  private function getWrappedQueue($name, $reliable = FALSE) {
    $class = variable_get(self::VAR_PREFIX . 'class_' . $name, NULL);
    if (!$class) {
      $class = variable_get(self::VAR_PREFIX . 'default_class', 'SystemQueue');
    }
    $object = new $class($name);
    if ($reliable && !$object instanceof DrupalReliableQueueInterface) {
      $class = variable_get(self::VAR_PREFIX . 'default_reliable_class', 'SystemQueue');
      $object = new $class($name);
    }
    return $object;
  }

  private function getInfoArray($op_name) {
    return array(
       'name' => $this->name,
       'op_name' => $op_name,
       'op_start' => microtime(TRUE),
    );
  }

  private function checkResult($results) {
    foreach ($results as $result) {
      if ($result === FALSE) {
        return FALSE;
      }
    }
    return TRUE;
  }

  public function claimItem($lease_time = 30) {
    $info = $this->getInfoArray(__FUNCTION__);
    $hooks_result = module_invoke_all('queue_claimitem_before', $info, $lease_time);

    // If any of the implementing modules returns FALSE no item is returned,
    // as if the queue was empty and had no more items left.
    if (!$this->checkResult($hooks_result)) {
      $result = FALSE;
    } else {
      $result = $this->queue->claimItem($lease_time);
    }

    $info['op_end'] = microtime(TRUE);
    module_invoke_all('queue_claimitem_after', $info, $lease_time, $result);
    return $result;
  }

  public function createItem($data) {
    $info = $this->getInfoArray(__FUNCTION__);
    module_invoke_all('queue_createitem_before', $info, $data);

    $result = $this->queue->createItem($data);

    $info['op_end'] = microtime(TRUE);
    module_invoke_all('queue_createitem_after', $info, $data, $result);
    return $result;
  }

  public function numberOfItems() {
    $info = $this->getInfoArray(__FUNCTION__);
    module_invoke_all('queue_numberofitems_before', $info);

    $result = $this->queue->numberOfItems();

    $info['op_end'] = microtime(TRUE);
    module_invoke_all('queue_numberofitems_after', $info, $result);
    return $result;
  }

  public function releaseItem($item) {
    $info = $this->getInfoArray(__FUNCTION__);
    module_invoke_all('queue_releaseitem_before', $info, $item);

    $result = $this->queue->releaseItem($item);

    $info['op_end'] = microtime(TRUE);
    module_invoke_all('queue_releaseitem_after', $info, $item, $result);
    return $result;
  }

  public function deleteItem($item) {
    $info = $this->getInfoArray(__FUNCTION__);
    module_invoke_all('queue_deleteitem_before', $info, $item);

    $result = $this->queue->deleteItem($item);

    $info['op_end'] = microtime(TRUE);
    module_invoke_all('queue_deleteitem_after', $info, $item, $result);
    return $result;
  }

  public function createQueue() {
    $info = $this->getInfoArray(__FUNCTION__);
    module_invoke_all('queue_createqueue_before', $info);

    $result = $this->queue->createQueue();

    $info['op_end'] = microtime(TRUE);
    module_invoke_all('queue_createqueue_after', $info);
    return $result;
  }

  public function deleteQueue() {
    $info = $this->getInfoArray(__FUNCTION__);
    module_invoke_all('queue_deletequeue_before', $info);

    $result = $this->queue->deleteQueue();

    $info['op_end'] = microtime(TRUE);
    module_invoke_all('queue_deletequeue_after', $info, $result);
    return $result;
  }

}
