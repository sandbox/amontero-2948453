<?php

/**
 * Implements hook_queue_class_info.
 *
 * @return array of class information definitions.
 */
function hooked_queue_queue_class_info() {
  return array(
    'HookedQueue' => array(
      'title' => t('Hooked Queue'),
      'inspect' => TRUE,
      'operations' => array(
        'view',
        'release',
        'delete',
        'deleteall',
      ),
    ),
  );
}

class QueueUIHookedQueue extends QueueUISystemQueue {}
